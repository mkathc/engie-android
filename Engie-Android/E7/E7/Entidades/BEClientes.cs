﻿using Android.Runtime;
using System;
using System.Collections.Generic;
using System.Text;

namespace E7.Entidades
{
    public class BEClientes
    {
        public string CodCliente { get; set; }
        public string NomCliente { get; set; }
        public JavaList<BEMedidor> MedCliente { get; set; }
        public JavaList<BEPuntoFacturacion> PFaCliente { get; set; }
    }
}
