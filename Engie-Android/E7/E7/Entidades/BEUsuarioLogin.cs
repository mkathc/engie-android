﻿using System;
using System.Collections.Generic;
using System.Text;

namespace E7.Entidades
{
    public class BEUsuarioLogin
    {
        public string Usuario { get; set; }
        public string UsuarioInterno { get; set; }        
        public string NombreCompleto { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string MensajeError { get; set; }
        public string ValidarOperacion { get; set; }
    }
}
