﻿using System;
using System.Collections.Generic;
using System.Text;

namespace E7.Entidades
{
    public class BEMedidor
    {
        public string CodCliente { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
    }
}
