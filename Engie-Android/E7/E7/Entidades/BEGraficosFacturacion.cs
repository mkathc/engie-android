﻿using System;
using System.Collections.Generic;
using System.Text;

namespace E7.Entidades
{
    public class BEGraficosFacturacion
    {
        public string Concepto { get; set; }
        public string Anio { get; set; }
        public string Mes { get; set; }
        public string Consumo { get; set; }
        public DateTime Fecha { get; set; }
    }
}
