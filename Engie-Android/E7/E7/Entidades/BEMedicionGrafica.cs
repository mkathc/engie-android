﻿using System;
using System.Collections.Generic;
using System.Text;

namespace E7.Entidades
{
    public class BEMedicionGrafica
    {
        public string Item { get; set; }
        public DateTime FechayHora { get; set; }
        public double EnergiaActiva { get; set; }
        public double Potencia { get; set; }
        public double EnergiaReactiva { get; set; }
        public string Tension { get; set; }
        public string Corriente { get; set; }
    }
}
