﻿using System;
using System.Collections.Generic;
using System.Text;

namespace E7.Entidades
{
    public class BEPuntoFacturacion
    {
        public string CodCliente { get; set; }
        public string ID { get; set; }
        public string Descripcion { get; set; }
    }
}
