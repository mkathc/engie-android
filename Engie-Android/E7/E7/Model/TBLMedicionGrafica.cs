﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace E7.Model
{
    public class TBLMedicionGrafica
    {
        public string CodCliente { get; set; }
        public string CodMedicion { get; set; }
        public byte[] BusquedaJson { get; set; }
    }
}