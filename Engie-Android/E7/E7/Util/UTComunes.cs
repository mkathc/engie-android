﻿using Android.App;
using Android.Content;
using Android.Net;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace E7.Util
{
    public class UTComunes
    {
        //Validación de Conexión a datos e internet
        public Task<bool> IsConnected()
        {
            return Task.Run(() =>
            {
                ConnectivityManager connectivityManager = (ConnectivityManager)Application.Context.GetSystemService(Context.ConnectivityService);
                NetworkInfo activeConnection = connectivityManager.ActiveNetworkInfo;
                return (activeConnection != null) && activeConnection.IsConnected;
            });
        }
    }
}
