﻿using E7.Entidades;
using E7.Util;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace E7.Servicio
{
    public class DALogin
    {
        public string ResultadoFinalAU { get; set; }
        public string ResultadoFinalUC { get; set; }
        public string ResultadoFinalEMT { get; set; }
        public string ResultadoFinalCC { get; set; }
        UTConfiguracion config = new UTConfiguracion();

        public Task<BEUsuarioLogin> AutenticarUsuario(string Usuario, string Clave)
        {
            #region Método Get UsuarioLogin
            
            return Task.Run(() =>
            {
                try
                {
                    BEUsuarioLogin entidad = null;
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(config.URLServicesLogin);
                        client.DefaultRequestHeaders.Clear();
                        client.DefaultRequestHeaders.Add(config.ApiKeyName,config.ApiKeyValues);

                        // Realizar la petición GET
                        HttpResponseMessage response = client.GetAsync("AutenticarUsuario?Usuario=" + Usuario + "&Clave=" + Clave).Result;
                        if (response.IsSuccessStatusCode)
                        {
                            var res = response.Content.ReadAsStringAsync().Result;
                            entidad = (BEUsuarioLogin)Newtonsoft.Json.JsonConvert.DeserializeObject<BEUsuarioLogin>(res);
                        }
                    }
                    return entidad;
                }
                catch (Exception ex)
                {
                    ResultadoFinalAU = ex.Message;
                    return null;
                }
            });
            #endregion
        }
        public Task<List<BEClientes>> Usuario_Cliente(string Usuario)
        {
            #region Método Get Clientes
            return Task.Run(() =>
            {
                try
                {
                    List<BEClientes> entidad = null;
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(config.URLServicesLogin);
                        client.DefaultRequestHeaders.Clear();
                        client.DefaultRequestHeaders.Add(config.ApiKeyName, config.ApiKeyValues);

                        // Realizar la petición GET
                        HttpResponseMessage response = client.GetAsync("Usuario_Cliente?Usuario=" + Usuario).Result;

                        if (response.IsSuccessStatusCode)
                        {
                            var res = response.Content.ReadAsStringAsync().Result;
                            entidad = (List<BEClientes>)Newtonsoft.Json.JsonConvert.DeserializeObject<List<BEClientes>>(res);
                        }
                    }
                    return entidad;
                }
                catch (Exception ex)
                {
                    ResultadoFinalUC = ex.Message;
                    return null;
                }
            });
            #endregion
        }
        public Task<BEUsuarioLogin> EnviarMailToken(string Usuario, string Token)
        {
            #region Método Get UsuarioLogin
            return Task.Run(() =>
            {
                try
                {
                    BEUsuarioLogin entidad = null;
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(config.URLServicesLogin);
                        client.DefaultRequestHeaders.Clear();
                        client.DefaultRequestHeaders.Add(config.ApiKeyName, config.ApiKeyValues);


                        // Realizar la petición GET
                        HttpResponseMessage response = client.GetAsync("EnviarMailToken?Usuario=" + Usuario + "&Token=" + Token).Result;

                        if (response.IsSuccessStatusCode)
                        {
                            var res = response.Content.ReadAsStringAsync().Result;
                            entidad = (BEUsuarioLogin)Newtonsoft.Json.JsonConvert.DeserializeObject<BEUsuarioLogin>(res);
                        }
                    }
                    return entidad;
                }
                catch (Exception ex)
                {
                    ResultadoFinalEMT = ex.Message;
                    return null;
                }
            });
            #endregion
        }
        public Task<BEUsuarioLogin> CambiarClave(string Usuario, string Clave)
        {
            #region Método Get UsuarioLogin
            return Task.Run(() =>
            {
                try
                {
                    BEUsuarioLogin entidad = null;
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(config.URLServicesLogin);
                        client.DefaultRequestHeaders.Clear();
                        client.DefaultRequestHeaders.Add(config.ApiKeyName, config.ApiKeyValues);

                        // Realizar la petición GET
                        HttpResponseMessage response = client.GetAsync("CambiarClave?Usuario=" + Usuario + "&Clave=" + Clave).Result;

                        if (response.IsSuccessStatusCode)
                        {
                            var res = response.Content.ReadAsStringAsync().Result;
                            entidad = (BEUsuarioLogin)Newtonsoft.Json.JsonConvert.DeserializeObject<BEUsuarioLogin>(res);
                        }
                    }
                    return entidad;
                }
                catch (Exception ex)
                {
                    ResultadoFinalCC = ex.Message;
                    return null;
                }
            });
            #endregion
        }
    }
}
