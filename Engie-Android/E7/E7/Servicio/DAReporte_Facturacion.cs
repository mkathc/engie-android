﻿using E7.Entidades;
using E7.Util;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace E7.Servicio
{
    public class DAReporte_Facturacion
    {
        public string ResultadoFinalRHF { get; set; }
        public string ResultadoFinalRDF { get; set; }
        public string ResultadoFinalRHP { get; set; }
        UTConfiguracion config = new UTConfiguracion();

        public Task<List<BEGraficosFacturacion>> Reporte_HistoricoFacturado(string FechaDesde, string FechaHasta, string CodCliente, string CodPuntoFacturacion, string Moneda)
        {
            #region Método Get Reporte Historico Facturado
            return Task.Run(() =>
            {
                try
                {

                    List<BEGraficosFacturacion> entidad = null;
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new System.Uri(config.URLServicesReporte);
                        client.DefaultRequestHeaders.Clear();
                        client.DefaultRequestHeaders.Add(config.ApiKeyName, config.ApiKeyValues);

                        // Realizar la petición GET
                        HttpResponseMessage response = client.GetAsync("ReporteF_HistoricoFacturado?FechaDesde=" + FechaDesde + "&FechaHasta=" + FechaHasta + "&CodCliente=" + CodCliente + "&CodPuntoFacturacion=" + CodPuntoFacturacion + "&Moneda=" + Moneda).Result;

                        if (response.IsSuccessStatusCode)
                        {
                            var res = response.Content.ReadAsStringAsync().Result;
                            entidad = (List<BEGraficosFacturacion>)Newtonsoft.Json.JsonConvert.DeserializeObject<List<BEGraficosFacturacion>>(res);
                        }
                    }
                    return entidad;
                }
                catch (Exception ex)
                {
                    ResultadoFinalRHF = ex.Message;
                    return null;
                }
            });
            #endregion  
        }
        public Task<List<BEGraficosFacturacion>> Reporte_HistoricoPrecio(string FechaDesde, string FechaHasta, string CodCliente, string CodPuntoFacturacion, string Moneda)
        {
            #region Método Get Reporte Historico de Precios
            return Task.Run(() =>
            {
                try
                {
                    List<BEGraficosFacturacion> entidad = null;
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new System.Uri(config.URLServicesReporte);
                        client.DefaultRequestHeaders.Clear();
                        client.DefaultRequestHeaders.Add(config.ApiKeyName, config.ApiKeyValues);

                        // Realizar la petición GET
                        HttpResponseMessage response = client.GetAsync("ReporteF_HistoricoPrecio?FechaDesde=" + FechaDesde + "&FechaHasta=" + FechaHasta + "&CodCliente=" + CodCliente + "&CodPuntoFacturacion=" + CodPuntoFacturacion + "&Moneda=" + Moneda).Result;

                        if (response.IsSuccessStatusCode)
                        {
                            var res = response.Content.ReadAsStringAsync().Result;
                            entidad = (List<BEGraficosFacturacion>)Newtonsoft.Json.JsonConvert.DeserializeObject<List<BEGraficosFacturacion>>(res);
                            //var n = Newtonsoft.Json.JsonConvert.SerializeObject(entidad);
                        }
                    }
                    return entidad;
                }
                catch (Exception ex)
                {
                    ResultadoFinalRHP = ex.Message;
                    return null;
                }
            });
            #endregion  
        }
        public Task<List<BEGraficosFacturacion>> Reporte_DistribucionFacturacion(string Fecha, string CodCliente, string CodPuntoFacturacion, string Moneda)
        {
            #region Método Get Reporte Distribucion Facturacion
            return Task.Run(() =>
            {
                try
                {
                    List<BEGraficosFacturacion> entidad = null;
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new System.Uri(config.URLServicesReporte);
                        client.DefaultRequestHeaders.Clear();
                        client.DefaultRequestHeaders.Add(config.ApiKeyName, config.ApiKeyValues);

                        // Realizar la petición GET
                        HttpResponseMessage response = client.GetAsync("ReporteF_DistribucionFacturacion?Fecha=" + Fecha + "&CodCliente=" + CodCliente + "&CodPuntoFacturacion=" + CodPuntoFacturacion + "&Moneda=" + Moneda).Result;

                        if (response.IsSuccessStatusCode)
                        {
                            var res = response.Content.ReadAsStringAsync().Result;
                            entidad = (List<BEGraficosFacturacion>)Newtonsoft.Json.JsonConvert.DeserializeObject<List<BEGraficosFacturacion>>(res);
                        }
                    }
                    return entidad;
                }
                catch (Exception ex)
                {
                    ResultadoFinalRDF = ex.Message;
                    return null;
                }
            });
            #endregion  
        }
    }
}
