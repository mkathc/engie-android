﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;


namespace E7.Droid
{
    [Activity(Label = "Cons2Activity", Theme = "@style/Theme.DesignDemo")]
    public class Cons2Activity : AppCompatActivity
    {
        DrawerLayout drawerLayout;
        NavigationView navigationView;
        ClientesAdapter adapter_c;
        MedidorAdapter adapter_m;
        JavaList<Clientes> clientes;
        JavaList<Medidor> medidores;
        Spinner CodCliente, CodMedidor;
        ImageButton Graficar;
        string _codCliente, _codMedidor;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Create your application here

            SetContentView(layoutResID: Resource.Layout.cons2);
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.menu);
            drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);



            CodCliente = FindViewById<Spinner>(Resource.Id.spinner1);
            CodMedidor = FindViewById<Spinner>(Resource.Id.spinner2);

            CodCliente.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(CodCliente_ItemSelected);
            CodMedidor.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(CodMedidor_ItemSelected);

            ListarClientes();

            void ListarClientes()
            {
                clientes = COLogin.ListarClientes();
                adapter_c = new ClientesAdapter(this, clientes);
                CodCliente.Adapter = adapter_c;
            }

            void CodCliente_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                _codCliente = clientes[e.Position].CodCliente;
                ListarMedidor(_codCliente);

            }
            void ListarMedidor(string _codCliente)
            {
                medidores = COLogin.ListarMedidor(_codCliente);
                adapter_m = new MedidorAdapter(this, medidores);
                CodMedidor.Adapter = adapter_m;

            }
            void CodMedidor_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                _codMedidor = medidores[e.Position].CodMedidor;
            }
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    drawerLayout.OpenDrawer(Android.Support.V4.View.GravityCompat.Start);
                    return true;
            }
            return base.OnOptionsItemSelected(item);
        }
    }
}