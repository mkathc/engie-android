using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace E7.Droid
{
    public class BEUsuarioLogin
    {
        public string Usuario { get; set; }
        public string NombreCompleto { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string MensajeError { get; set; }
        public string ValidarOperacion { get; set; }
        public List<Clientes> Usuario_cliente { get; set; }
    }
    public class Clientes
    {
        public string CodCliente { get; set; }
        public string NomCliente { get; set; }
        public List<Medidor> MedCliente { get; set; }
        public List<PuntoFacturacion> PFaCliente { get; set; }
    }
    public class Medidor
    {
        public string CodMedidor { get; set; }
        public string DescMedidor { get; set; }
    }
    public class PuntoFacturacion
    {
        public string IdPuntoFacturacion { get; set; }
        public string DescPuntoFacturacion { get; set; }
    }
}
