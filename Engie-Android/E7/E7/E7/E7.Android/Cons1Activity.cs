﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;

namespace E7.Droid
{
    [Activity(Label = "Cons1Activity", Theme = "@style/Theme.DesignDemo")]
    public class Cons1Activity : AppCompatActivity
    {
        DrawerLayout drawerLayout;
        NavigationView navigationView;
        ClientesAdapter adapter_c;
        PuntoFacturacionAdapter adapter_p;
        JavaList<Clientes> clientes;
        JavaList<PuntoFacturacion> puntoFacturacion;
        Spinner CodCliente, Anio, IdPuntoFacturacion;
        private ArrayAdapter<string> Adaptador;
        ImageButton Graficar;
        string _codCliente, _codPuntoF, _anio;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Create your application here

            SetContentView(layoutResID: Resource.Layout.cons1);
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.menu);
            drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);


            CodCliente = FindViewById<Spinner>(Resource.Id.spinner1);
            Anio = FindViewById<Spinner>(Resource.Id.spinner2);
            IdPuntoFacturacion = FindViewById<Spinner>(Resource.Id.spinner3);
            Graficar = FindViewById<ImageButton>(Resource.Id.imageButton1);

            CodCliente.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(CodCliente_ItemSelected);
            Anio.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(Anio_ItemSelected);
            IdPuntoFacturacion.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(IdPuntoFacturacion_ItemSelected);

            ListarClientes();
            ListarAnio();

            Graficar.Click += (sender, e) => Graficos();

            void Graficos()
            {
                if (!string.IsNullOrEmpty(_codCliente) && !string.IsNullOrEmpty(_anio) && !string.IsNullOrEmpty(_codPuntoF))
                {
                    var intent_data = new Intent(this, typeof(graf));
                    intent_data.PutExtra("_codCliente", _codCliente);
                    intent_data.PutExtra("_anio", _anio);
                    intent_data.PutExtra("_codPuntoF", _codPuntoF);
                    StartActivity(intent_data);
                }
                else if (string.IsNullOrEmpty(_codCliente))
                {
                    string toast = "Seleccione el cliente";
                    Toast.MakeText(this, toast, ToastLength.Short).Show();
                }
                else if (string.IsNullOrEmpty(_anio))
                {
                    string toast = "Seleccione el año";
                    Toast.MakeText(this, toast, ToastLength.Short).Show();
                }
                else
                {
                    string toast = "Seleccione un punto de facturación";
                    Toast.MakeText(this, toast, ToastLength.Short).Show();
                }

            }

            //Cargar Data Cliente
            void ListarClientes()
            {
                _codPuntoF = null;
                clientes = COLogin.ListarClientes();
                adapter_c = new ClientesAdapter(this, clientes);
                CodCliente.Adapter = adapter_c;
                CodCliente.ItemSelected += CodCliente_ItemSelected;
            }

            void CodCliente_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                _codCliente = clientes[e.Position].CodCliente;
                ListarPuntoFacturacion(_codCliente);
            }
            void ListarAnio()
            {
                string[] items = new[] { "2017", "2016", "2015" };
                var Adaptador = new ArrayAdapter<string>(this, Android.Resource.Layout.SelectDialogItem, items);
                Anio.Adapter = Adaptador;
            }

            void Anio_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                Spinner spinner = (Spinner)sender;

                _anio = spinner.GetItemAtPosition(e.Position).ToString();
            }
            void ListarPuntoFacturacion(string _codCliente)
            {
                puntoFacturacion = COLogin.ListarPuntoFacturacion(_codCliente);
                adapter_p = new PuntoFacturacionAdapter(this, puntoFacturacion);
                IdPuntoFacturacion.Adapter = adapter_p;
            }
            void IdPuntoFacturacion_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                _codPuntoF = puntoFacturacion[e.Position].IdPuntoFacturacion;
            }

        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    drawerLayout.OpenDrawer(Android.Support.V4.View.GravityCompat.Start);
                    return true;
            }
            return base.OnOptionsItemSelected(item);
        }
    }
}
