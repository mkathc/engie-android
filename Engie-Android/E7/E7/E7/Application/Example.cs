﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Reflection;


namespace E7.Application
{
    public class Example
    {
        public Type ExampleType { get; }
        public string Title { get; }
        public string Description { get; }
        public ExampleIcon? Icon { get; }

        public Example(Type exampleType)
        {
            ExampleType = exampleType;

            var attribute = exampleType.GetCustomAttributes<ExampleDefinition>().Single();

            Title = attribute.Title;
            Description = attribute.Description;
            Icon = attribute.Icon;
        }
    }
}
