﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace E7.Droid
{
    [Activity(Label = "grac2", Theme = "@style/Theme.DesignDemo")]
    public class grac2 : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(layoutResID: Resource.Layout.grac2);
        }
    }
}