﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ML;

namespace E7.Droid.Utiles
{
    public class Mes
    {
        public string IdMes { get; set; }
        public string NomMes { get; set; }
    }
    public class COMes {
        public static JavaList<Mes> ListarMes()
        {
            JavaList<Mes> listar = new JavaList<Mes>();

            listar.Add(new Mes() { IdMes = "01", NomMes = "ENERO" });
            listar.Add(new Mes() { IdMes = "02", NomMes = "FEBRERO" });
            listar.Add(new Mes() { IdMes = "03", NomMes = "MARZO" });
            listar.Add(new Mes() { IdMes = "04", NomMes = "ABRIL" });
            listar.Add(new Mes() { IdMes = "05", NomMes = "MAYO" });
            listar.Add(new Mes() { IdMes = "06", NomMes = "JUNIO" });
            listar.Add(new Mes() { IdMes = "07", NomMes = "JULIO" });
            listar.Add(new Mes() { IdMes = "08", NomMes = "AGOSTO" });
            listar.Add(new Mes() { IdMes = "09", NomMes = "SEPTIEMBRE" });
            listar.Add(new Mes() { IdMes = "10", NomMes = "OCTUBRE" });
            listar.Add(new Mes() { IdMes = "11", NomMes = "NOVIEMBRE" });
            listar.Add(new Mes() { IdMes = "12", NomMes = "DICIEMBRE" });

            return listar;
        }
    }

    class MesAdapter : BaseAdapter
    {
        private Context c;
        private JavaList<Mes> mes;
        private LayoutInflater inflater;

        public MesAdapter(Context c, JavaList<Mes> mes)
        {
            this.c = c;
            this.mes = mes;
        }
        public override Java.Lang.Object GetItem(int position)
        {
            return mes.Get(position);
        }
        public override long GetItemId(int position)
        {
            return position;
        }
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            if (inflater == null)
            {
                inflater = (LayoutInflater)c.GetSystemService(Context.LayoutInflaterService);
            }
            if (convertView == null)
            {
                convertView = inflater.Inflate(Resource.Layout.ResourceSpinner, parent, false);
            }

            TextView cod = convertView.FindViewById<TextView>(Resource.Id.cod1);
            TextView nomb = convertView.FindViewById<TextView>(Resource.Id.nom1);
            cod.Text = mes[position].IdMes;
            nomb.Text = mes[position].NomMes;
            return convertView;
        }
        public override int Count
        {
            get { return mes.Size(); }
        }
    }

}