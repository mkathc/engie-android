﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using SciChart.Charting.Visuals;
using SciChart.Charting.Visuals.Axes;
using SciChart.Data.Model;
using SciChart.Charting.Model.DataSeries;
using SciChart.Charting.Visuals.RenderableSeries;
using SciChart.Drawing.Common;
using SciChart.Charting.Visuals.PointMarkers;
using Android.Graphics;
using SciChart.Charting.Modifiers;
using SciChart.Core.Framework;
using E7.Droid;
using Android.Util;
using E7.Droid.Utiles;
using E7.Droid.Usuario;
using System.Threading.Tasks;
using E7.Util;
using System.Threading;

namespace E7.Droid
{
    [Activity(Label = "Engie", MainLauncher = true, Icon = "@drawable/icon", Theme = "@style/Theme.DesignDemo", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class MainActivity : Activity
    {


        UTComunes conexion = new UTComunes();
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            //Inicio de Sesión
            //var intent = new Intent(this, typeof(Login));
            //StartActivity(intent);
            //
            //Licencia
            SciChart.Charting.Visuals.SciChartSurface.SetRuntimeLicenseKey(@"<LicenseContract>
                <Customer>Engie Energía Peru S.A.</Customer>
                <OrderId>ABT171006-4035-82164</OrderId>
                <LicenseCount>1</LicenseCount>
                <IsTrialLicense>false</IsTrialLicense>
                <SupportExpires>10/06/2018 00:00:00</SupportExpires>
                <ProductCode>SC-IOS-ANDROID-2D-ENTERPRISE-SRC</ProductCode>
                <KeyCode>eacf520a838012385f87b1ad8f6d0fc95a489c67679d2211b3abd16dff13e874586b326a035886f3540692eacd82f3691541102690a2ddd590c1969087691d8f904a579a296d486c962cd708769efa18c3a3f500adcb425e34b937c172c2cab5b91b2b1572a16dd9cd6c7518db512aa6b80b1f2e85091b7df1f7966a9cc739f9b9d0675a92fd58e2eef5847da3e00ae39de672024d6f3cc4b4a975ee27becd28efda88febe4886f6b1dce0f5e0f6ea18eeaa97b3eb3acd8518454c1fc8a245148dd76b27</KeyCode>
                </LicenseContract>");
            new Thread(new ThreadStart(delegate
            {
                RunOnUiThread(async () => await ValidarInicioSesion());
            })).Start();

        }

        async Task ValidarInicioSesion()
        {
            //Validación de cuenta iniciada
            ISharedPreferences pref = Application.Context.GetSharedPreferences("UserInfo", FileCreationMode.Private);
            string usuario = pref.GetString("Username", String.Empty);
            string clave = pref.GetString("Password", String.Empty);
            string _nombreCompleto = pref.GetString("_nombreCompleto", String.Empty);
            string filtro = pref.GetString("filtro", String.Empty);

            if (usuario == String.Empty || clave == String.Empty)
            {
                Intent intent = new Intent(this, typeof(Login));
                this.StartActivity(intent);
            }

            else
            {
                if (!string.IsNullOrEmpty(usuario) && !string.IsNullOrEmpty(clave))
                {
                    Intent intent = new Intent(this, typeof(PrincipalFragment));
                    intent.PutExtra("_nombreCompleto", _nombreCompleto);
                    intent.PutExtra("filtro", filtro);
                    this.StartActivity(intent);
                }
                else
                {
                    ISharedPreferencesEditor edit = pref.Edit();
                    edit.Clear();
                    edit.Apply();
                    Intent intent = new Intent(this, typeof(Login));
                    this.StartActivity(intent);
                    this.Finish();
                }
            }
        }

    }
}


