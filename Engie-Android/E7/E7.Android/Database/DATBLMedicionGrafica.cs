﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SQLite;
using Android.Util;
using System.Threading.Tasks;
using E7.Model;

namespace E7.Droid.Database
{
    public class DATBLMedicionGrafica
    {
        string folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

        public Task<bool> CreateTableMG()
        {
            return Task.Run(() =>
            {
                try
                {
                    using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Extranet.db")))
                    {
                        connection.CreateTable<TBLMedicionGrafica>();
                        return true;
                    }
                }
                catch (SQLiteException ex)
                {
                    Log.Info("SQLiteEx", ex.Message);
                    return false;
                }
            });
        }

        public Task<bool> InsertTableMG(TBLMedicionGrafica mg)
        {
            return Task.Run(() =>
            {
                try
                {
                    using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Extranet.db")))
                    {
                        connection.Insert(mg);
                        return true;
                    }
                }
                catch (SQLiteException ex)
                {
                    Log.Info("SQLiteEx", ex.Message);
                    return false;
                }
            });
        }

        public Task<List<TBLMedicionGrafica>> SelectTableMG(TBLMedicionGrafica hf)
        {
            return Task.Run(() =>
            {
                try
                {
                    List<TBLMedicionGrafica> list = new List<TBLMedicionGrafica>();
                    using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Extranet.db")))
                    {
                        list = connection.Query<TBLMedicionGrafica>("SELECT * FROM TBLMedicionGrafica WHERE CodCliente=? AND CodMedicion=?", hf.CodCliente, hf.CodMedicion);
                        return list;

                    }
                }
                catch (SQLiteException ex)
                {
                    Log.Info("SQLiteEx", ex.Message);
                    return null;
                }
            });
        }

        public Task<List<TBLMedicionGrafica>> ValidateTableMG()
        {
            return Task.Run(() =>
            {
                try
                {
                    List<TBLMedicionGrafica> list = new List<TBLMedicionGrafica>();
                    using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Extranet.db")))
                    {
                        list = connection.Table<TBLMedicionGrafica>().ToList();
                        return list;
                    }
                }
                catch (SQLiteException ex)
                {
                    Log.Info("SQLiteEx", ex.Message);
                    return null;
                }
            });
        }

        public Task<bool> DeleteTableMG(string CodCliente, string CodMedicion)
        {
            return Task.Run(() =>
            {
                try
                {
                    using (var connection = new SQLiteConnection(System.IO.Path.Combine(folder, "Extranet.db")))
                    {
                        connection.Query<TBLMedicionGrafica>("DELETE FROM TBLMedicionGrafica WHERE CodCliente=? AND CodMedicion=?", CodCliente, CodMedicion);
                        return true;
                    }
                }
                catch (SQLiteException ex)
                {
                    Log.Info("SQLiteEx", ex.Message);
                    return false;
                }
            });
        }
    }
}