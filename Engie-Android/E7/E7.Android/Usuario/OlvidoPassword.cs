﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using System.Threading;
using E7.Servicio;
using E7.Entidades;
using E7.Droid.Utiles;
using E7.Util;
using ML;

namespace E7.Droid.Usuario
{
    [Activity(Label = "FpassActivity", Theme = "@style/Theme.DesignDemo", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class OlvidoPassword : Activity
    {
        Button Cancelar, Restaurar;
        TextView Usuario;
        ProgressDialog progress;
        DALogin conexion = new DALogin();
        BEUsuarioLogin resultado;
        string _codigo;
        UTComunes util = new UTComunes();
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Create your application here

            SetContentView(layoutResID: Resource.Layout.OlvidoPassword);

            Cancelar = FindViewById<Button>(Resource.Id.button1);
            Restaurar = FindViewById<Button>(Resource.Id.button2);
            Usuario = FindViewById<TextView>(Resource.Id.editText1);

            Restaurar.Click += (sender, e) => ncode();
            Cancelar.Click += (sender, e) => login();

            void Cargando(string mensaje)
            {
                progress = ProgressDialog.Show(this, "", mensaje, true, false);
                progress.SetProgressStyle(ProgressDialogStyle.Spinner);
            }
            void ncode()
            {
                if (!string.IsNullOrEmpty(Usuario.Text))
                {
                    Cargando("Generando código...");
                    new Thread(new ThreadStart(delegate
                    {
                        RunOnUiThread(async () => await GenerarToken());
                    })).Start();
                }
                else
                {
                    Toast.MakeText(this, "Ingrese el usuario", ToastLength.Long).Show();
                }
            }

            void login()
            {
                Finish();
                StartActivity(typeof(Login));
            }
            async Task EnviarToken()
            {
                var IsConnected = await util.IsConnected();
                if (IsConnected == true)
                {
                    resultado = await conexion.EnviarMailToken(Usuario.Text, _codigo);
                    if (resultado.ValidarOperacion == "true")
                    {
                        progress.Dismiss();
                        Toast.MakeText(this, resultado.MensajeError, ToastLength.Long).Show();
                        var intent_data = new Intent(this, typeof(NuevoCodigo));
                        intent_data.PutExtra("_usuario", resultado.Usuario);
                        intent_data.PutExtra("_email", resultado.Email);
                        intent_data.PutExtra("_codigo", _codigo);
                        StartActivity(intent_data);
                    }
                    else
                    {
                        progress.Dismiss();
                        Toast.MakeText(this, resultado.MensajeError, ToastLength.Long).Show();
                    }
                }
                else if (IsConnected == false)
                {
                    Toast.MakeText(this, "En estos momentos no tiene servicio de internet, por favor verificar.", ToastLength.Long).Show();
                    progress.Dismiss();
                }

            }
            async Task GenerarToken()
            {
                RandomPassword vl_RandomPassword = new RandomPassword();
                _codigo = await vl_RandomPassword.Generate();
                if (_codigo != null)
                {
                    progress.Dismiss();
                    Cargando("Enviado código...");
                    new Thread(new ThreadStart(delegate
                    {
                        RunOnUiThread(async () => await EnviarToken());
                    })).Start();
                }
                else {
                    Toast.MakeText(this, "Inténtelo nuevamente", ToastLength.Long).Show();
                }
            }

        }
    }
}