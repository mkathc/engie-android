﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.Graphics;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading;
using System.Threading.Tasks;
using E7.Servicio;
using E7.Entidades;
using ML;
using E7.Util;
using ML.Usuario;

namespace E7.Droid.Usuario
{
    [Activity(Label = "LoginActivity", Theme = "@style/Theme.DesignDemo", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class Login : Activity
    {
        BEUsuarioLogin resultado;
        EditText usuario, clave;
        Button ingresar, btn2;
        ProgressDialog progress;
        Thread thread = null;
        string filtro;
        CheckBox checkBox1;
        DALogin conexion = new DALogin();
        UTComunes util = new UTComunes();
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Create your application here

            SetContentView(layoutResID: Resource.Layout.Login);

            ingresar = FindViewById<Button>(Resource.Id.button1);
            usuario = FindViewById<EditText>(Resource.Id.editText1);
            clave = FindViewById<EditText>(Resource.Id.editText2);
            btn2 = FindViewById<Button>(Resource.Id.button2);
            checkBox1 = FindViewById<CheckBox>(Resource.Id.checkBox1);

            #if DEBUG
            usuario.Text = "vitpower2";
            clave.Text = "12345";
            //usuario.Text = "cliente_pruebapp";
            //clave.Text = "Engie1234";
            //usuario.Text = "engie_comercial";
            //clave.Text = "Engie2018$";
			#endif



            //Mensaje de ProgressBar
            void Cargando(string mensaje)
            {
                progress = ProgressDialog.Show(this, "", mensaje, true, false);
                progress.SetProgressStyle(ProgressDialogStyle.Spinner);
            }
            ingresar.Click += (sender, e) => CargarLogin();

            //Mensaje de Validaciones
            void CargarLogin()
            {
                if (!string.IsNullOrEmpty(usuario.Text) && !string.IsNullOrEmpty(clave.Text))
                {
                    Cargando("Iniciando Sesión...");
                    new Thread(new ThreadStart(delegate
                    {
                        RunOnUiThread(async () => await Login());
                    })).Start();
                }
                else if (string.IsNullOrEmpty(usuario.Text))
                {
                    usuario.RequestFocus();
                    string mensaje = "Ingrese el usuario";
                    Toast.MakeText(this, mensaje, ToastLength.Long).Show();
                }
                else
                {
                    clave.RequestFocus();
                    string mensaje = "Ingrese la contraseña";
                    Toast.MakeText(this, mensaje, ToastLength.Long).Show();
                }
            }
            btn2.Click += (sender, e) => fpass();

            async Task Login()
            {
                var IsConnected = await util.IsConnected();
                if (IsConnected == true)
                {
                    resultado = await conexion.AutenticarUsuario(usuario.Text.Trim(), clave.Text);
                    string MensajeErrorL = conexion.ResultadoFinalAU;                    

                    if (resultado == null)
                    {
                        progress.Dismiss();
                        Toast.MakeText(this, MensajeErrorL, ToastLength.Long).Show();
                    }
                    else
                    {
                        if (resultado.ValidarOperacion == "true")
                        {
                            progress.Dismiss();
                            Cargando("Obteniendo datos...");
                            new Thread(new ThreadStart(delegate
                            {
                                RunOnUiThread(async () => await CargarClientes());
                            })).Start();
                        }
                        else
                        {
                            progress.Dismiss();
                            Toast.MakeText(this, resultado.MensajeError, ToastLength.Long).Show();
                        }
                    }

                }
                else
                {
                    progress.Dismiss();
                    Toast.MakeText(this, "En estos momentos no tiene servicio de internet, por favor verificar.", ToastLength.Long).Show();
                }
            }
            async Task CargarClientes()
            {
                var main = MainViewModel.GetInstance();
                var _client = await conexion.Usuario_Cliente(resultado.UsuarioInterno);
                main.Clientes = _client;
                string MensajeErrorC = conexion.ResultadoFinalCC;
                if (_client == null)
                {
                    progress.Dismiss();
                    Toast.MakeText(this, MensajeErrorC, ToastLength.Long).Show();
                }
                else
                {
                    filtro = Newtonsoft.Json.JsonConvert.SerializeObject(_client);
                    if (filtro != "[]")
                    {
                        var intent_data = new Intent(this, typeof(PrincipalFragment));
                        intent_data.PutExtra("_nombreCompleto", resultado.NombreCompleto);
                        intent_data.PutExtra("filtro", filtro);
                        StartActivity(intent_data);
                        progress.Dismiss();

                        //CheckBox Mantener sesión
                        if (checkBox1.Checked)
                        {
                            ISharedPreferences pref = Application.Context.GetSharedPreferences("UserInfo", FileCreationMode.Private);
                            ISharedPreferencesEditor edit = pref.Edit();
                            edit.PutString("Username", usuario.Text.Trim());
                            edit.PutString("Password", clave.Text.Trim());
                            edit.PutString("_nombreCompleto", resultado.NombreCompleto);
                            edit.PutString("filtro", filtro);
                            edit.Apply();
                        }
                        else
                        {
                            ISharedPreferences pref = Application.Context.GetSharedPreferences("UserInfo", FileCreationMode.Private);
                            ISharedPreferencesEditor edit = pref.Edit();
                            edit.Clear();
                            edit.Apply();
                        }
                        //Limpiar
                        usuario.Text = "";
                        clave.Text = "";
                    }
                    else
                    {
                        Toast.MakeText(this, "No se tiene asignado ningún cliente", ToastLength.Long).Show();
                        progress.Dismiss();
                    }

                }
            }
        }



        public override void OnBackPressed()
        {
            RunOnUiThread(
                async () =>
                {
                    var isCloseApp = await AlertAsync(this, "Engie", "¿Deseas cerrar la aplicación?", "Si", "No");

                    if (isCloseApp)
                    {
                        var activity = (Activity)this;
                        activity.FinishAffinity();
                    }
                });
        }

        public Task<bool> AlertAsync(Context context, string title, string message, string positiveButton, string negativeButton)
        {
            var tcs = new TaskCompletionSource<bool>();

            using (var db = new AlertDialog.Builder(context))
            {
                db.SetTitle(title);
                db.SetMessage(message);
                db.SetPositiveButton(positiveButton, (sender, args) => { tcs.TrySetResult(true); });
                db.SetNegativeButton(negativeButton, (sender, args) => { tcs.TrySetResult(false); });
                db.Show();
            }

            return tcs.Task;
        }

        void fpass()
        {
            StartActivity(typeof(OlvidoPassword));
        }
    }
}

