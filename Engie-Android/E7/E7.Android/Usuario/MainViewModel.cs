﻿using System;
using System.Collections.Generic;
using E7.Entidades;

namespace ML.Usuario
{
    public class MainViewModel
    {
        #region Attributes
        public List<BEClientes> Clientes { get; set; }
        #endregion

        #region Constructor
        public MainViewModel()
        {
            instance = this;
        }
        #endregion

        #region Singleton
        static MainViewModel instance;

        public static MainViewModel GetInstance()
        {
            if (instance == null)
            {
                return new MainViewModel();
            }

            return instance;
        }
        #endregion
    }
}
