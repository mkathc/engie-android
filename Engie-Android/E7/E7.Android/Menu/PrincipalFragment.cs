﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Android.Graphics.Drawables.Shapes;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using System.Threading;
using E7.Droid.Consumo;
using E7.Droid.Usuario;
using E7.Droid.Facturacion;
using E7.Droid.OffLine;
using E7.Model;
using ML;

namespace E7.Droid
{
    [Activity(Label = "PrincipalFragment", Theme = "@style/Theme.DesignDemo", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class PrincipalFragment : AppCompatActivity
    {
        DrawerLayout drawerLayout;
        NavigationView navigationView;
        Inicio inicioFragment;
        Offline offlinefragment;
        CerrarSesion Csesionfragment;
        PreFacturacion facturacionFragment;
        PreConsumo consumoFragment;
        string _nombreCompleto, filtro;
        V7Toolbar toolbar;
        bool _inicioSet = false;
        TBLDistribucionFacturada df;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Create your application here
            
            _nombreCompleto = Intent.GetStringExtra("_nombreCompleto") ?? "Data not available";
            filtro = Intent.GetStringExtra("filtro") ?? "Data not available";

            SetContentView(layoutResID: Resource.Layout.PrincipalFragment);
            toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.Title = "Engie";
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.menu);

            drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);

            CreateFragments();
            LoadInditialFragment();

            SwitchFragment(Resource.Id.home);
            navigationView.NavigationItemSelected += NavigationView_NavigationItemSelected;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    drawerLayout.OpenDrawer(Android.Support.V4.View.GravityCompat.Start);
                    return true;
            }
            return base.OnOptionsItemSelected(item);
        }

        void NavigationView_NavigationItemSelected(object sender, NavigationView.NavigationItemSelectedEventArgs e)
        {
            SwitchFragment(e.MenuItem.ItemId);
            drawerLayout.CloseDrawers();
        }

        private void CreateFragments()
        {
            inicioFragment = new Inicio(_nombreCompleto, filtro);
            inicioFragment.CallConsumo += InicioFragment_CallConsumo;
            inicioFragment.CallFacturacion += InicioFragment_CallFacturacion;
            consumoFragment = new PreConsumo(_nombreCompleto, filtro);
            consumoFragment.CallConsumoFacturado += ConsumoFragment_CallConsumoFacturado;
            consumoFragment.CallMedicionYGraficas += ConsumoFragment_CallMedicionYGraficas;
            facturacionFragment = new PreFacturacion(_nombreCompleto, filtro);
            facturacionFragment.CallDistribucionFacturada += FacturacionFragment_CallDistribucionFacturada;
            facturacionFragment.CallGraficoHistoricoFacturado += FacturacionFragment_CallGraficoHistoricoFacturado;
            facturacionFragment.CallGraficoHistoricoPrecios += FacturacionFragment_CallGraficoHistoricoPrecios;
            Csesionfragment = new CerrarSesion(_nombreCompleto, filtro);
            Csesionfragment.CallCsesion += Csesionfragment_CallCsesion;
        }

        void Csesionfragment_CallCsesion(object sender, EventArgs e)
        {
            //SwitchFragment(Resource.Id.logout);
        }

        void InicioFragment_CallConsumo(object sender, EventArgs e)
        {
            SwitchFragment(Resource.Id.con);
        }

        void InicioFragment_CallFacturacion(object sender, EventArgs e)
        {
            SwitchFragment(Resource.Id.fac);
        }
        void ConsumoFragment_CallConsumoFacturado(object sender, EventArgs e)
        {
            var intent = new Intent(this, typeof(MediGrafica_F));
            intent.PutExtra("_nombreCompleto", _nombreCompleto);
            intent.PutExtra("filtro", filtro);
            StartActivity(intent);
        }

        void ConsumoFragment_CallMedicionYGraficas(object sender, EventArgs e)
        {
            var intent = new Intent(this, typeof(ConsFacturado_F));
            intent.PutExtra("_nombreCompleto", _nombreCompleto);
            intent.PutExtra("filtro", filtro);
            StartActivity(intent);
        }

        void FacturacionFragment_CallDistribucionFacturada(object sender, EventArgs e)
        {
            var intent = new Intent(this, typeof(DistFacturacion_F));
            intent.PutExtra("_nombreCompleto", _nombreCompleto);
            intent.PutExtra("filtro", filtro);
            StartActivity(intent);
        }

        void FacturacionFragment_CallGraficoHistoricoFacturado(object sender, EventArgs e)
        {
            var intent = new Intent(this, typeof(HistFacturado_F));
            intent.PutExtra("_nombreCompleto", _nombreCompleto);
            intent.PutExtra("filtro", filtro);
            StartActivity(intent);
        }

        void FacturacionFragment_CallGraficoHistoricoPrecios(object sender, EventArgs e)
        {
            var intent = new Intent(this, typeof(HistPrecio_F));
            intent.PutExtra("_nombreCompleto", _nombreCompleto);
            intent.PutExtra("filtro", filtro);
            StartActivity(intent);
        }

        private void LoadInditialFragment()
        {
            var transaction = FragmentManager.BeginTransaction();
            transaction.Add(Resource.Id.fragmentContainer, inicioFragment).Hide(inicioFragment);
            transaction.Add(Resource.Id.fragmentContainer, consumoFragment).Hide(consumoFragment);
            transaction.Add(Resource.Id.fragmentContainer, facturacionFragment).Hide(facturacionFragment);
            //transaction.Add(Resource.Id.fragmentContainer, offlinefragment).Hide(offlinefragment);
            transaction.Add(Resource.Id.fragmentContainer, Csesionfragment).Hide(Csesionfragment);
            transaction.Commit();
        }

        private void SwitchFragment(int FragmentId)
        {
            var transaction = FragmentManager.BeginTransaction();

            switch (FragmentId)
            {
                case Resource.Id.home:
                    transaction.Show(inicioFragment);
                    transaction.Hide(consumoFragment);
                    transaction.Hide(facturacionFragment);
                    //transaction.Hide(offlinefragment);
                    toolbar.SetBackgroundResource(Resource.Drawable.mylink);
                    //ActionBar.Title = "Consumo";
                    transaction.Commit();
                    break;

                case Resource.Id.con:
                    transaction.Show(consumoFragment);
                    transaction.Hide(inicioFragment);
                    transaction.Hide(facturacionFragment);
                    //transaction.Hide(offlinefragment);
                    toolbar.Background = null;
                    toolbar.SetBackgroundColor(Android.Graphics.Color.Rgb(0, 89, 158));
                    transaction.Commit();
                    break;

                case Resource.Id.fac:
                    transaction.Show(facturacionFragment);
                    transaction.Hide(inicioFragment);
                    transaction.Hide(consumoFragment);
                    //transaction.Hide(offlinefragment);
                    toolbar.Background = null;
                    toolbar.SetBackgroundColor(Android.Graphics.Color.Rgb(167, 37, 28));
                    transaction.Commit();
                    break;
                case Resource.Id.off:
                    //transaction.Show(offlinefragment);
                    Intent intentoff = new Intent(this, typeof(Offline));
                    intentoff.PutExtra("_nombreCompleto", _nombreCompleto);
                    intentoff.PutExtra("filtro", filtro);
                    StartActivity(intentoff);
                    break;

                case Resource.Id.csesion:
                    Intent intent = new Intent(this, typeof(Login));
                    intent.AddFlags(ActivityFlags.ClearTask);
                    intent.AddFlags(ActivityFlags.NewTask);
                    ISharedPreferences pref = Application.Context.GetSharedPreferences("UserInfo", FileCreationMode.Private);
                    ISharedPreferencesEditor edit = pref.Edit();
                    edit.PutString("Username", String.Empty);
                    edit.PutString("Password", String.Empty);
                    edit.PutString("_nombreCompleto", String.Empty);
                    edit.PutString("filtro", filtro);
                    edit.Apply();
                    StartActivity(intent);
                    break;
            }
        }

        public override void OnBackPressed()
        {
            if (_inicioSet)
                base.OnBackPressed();
            else

                SwitchFragment(Resource.Id.home);
        }
    }
}
