﻿using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Text;
using Android.Views;
using Android.Widget;
using E7.Droid.Database;
using E7.Entidades;
using E7.Model;
using E7.Servicio;
using ML;
using OxyPlot;
using OxyPlot.Series;
using OxyPlot.Xamarin.Android;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;

namespace E7.Droid.Facturacion
{
    [Activity(Label = "", Theme = "@style/Theme.DesignDemo", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class piechart : AppCompatActivity
    {
        private PlotView plotViewModel;
        private TableLayout mLLayoutModel;
        public PlotModel MyModel { get; set; }
        string[] colors = new string[] { "#7cb5ec", "#434348", "#90ed7d", "#f7a35c", "#8085e9", "#f15c80", "#e4d354", "#2b908f" };
        Android.App.ProgressDialog progress;
        string _usuarioInterno, filtro;
        TextView ToolOff, TitleMes;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.DistFacturacion_G);
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(GetDrawable(Resource.Drawable.backButton));

            ToolOff = FindViewById<TextView>(Resource.Id.modooff);
            TitleMes = FindViewById<TextView>(Resource.Id.TittleDF);
            plotViewModel = FindViewById<PlotView>(Resource.Id.plotViewModel);
            //mLLayoutModel = FindViewById<LinearLayout>(Resource.Id.linearLayoutModel);
            mLLayoutModel = FindViewById<TableLayout>(Resource.Id.linearLayoutModel);

            //Model Allocation Pie char
            var plotModel2 = new PlotModel();
            var pieSeries2 = new PieSeries()
            {
                StrokeThickness = 0,
                InsideLabelPosition = -5,
                AngleSpan = 360,
                StartAngle = 270,
                OutsideLabelFormat = null
            };

            _usuarioInterno = Intent.GetStringExtra("_usuarioInterno") ?? "Data not available";
            filtro = Intent.GetStringExtra("filtro") ?? "Data not available";
            string _codCliente = Intent.GetStringExtra("_codCliente") ?? "Data not available";
            string _codPuntoF = Intent.GetStringExtra("_codPuntoF") ?? "Data not available";
            string _fecha = Intent.GetStringExtra("_fecha") ?? "Data not available";
            string _modooff = Intent.GetStringExtra("_modooff") ?? "0";

            string _moneda = "USD";

            ModoOff();
            Inicializar();
            void ModoOff()
            {
                if (_modooff == "0")
                {
                    ToolOff.Visibility = ViewStates.Gone;
                }
                else
                {
                    ToolOff.Visibility = ViewStates.Visible;
                }
            }

            string TitleMesAnio(string fecha)
            {
                string MesLabel = "";
                DateTime title = DateTime.ParseExact(fecha, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                switch (title.Month)
                {
                    case 1:
                        MesLabel = "Enero " + title.Year;
                        break;
                    case 2:
                        MesLabel = "Febrero " + title.Year;
                        break;
                    case 3:
                        MesLabel = "Marzo " + title.Year;
                        break;
                    case 4:
                        MesLabel = "Abril " + title.Year;
                        break;
                    case 5:
                        MesLabel = "Mayo " + title.Year;
                        break;
                    case 6:
                        MesLabel = "Junio " + title.Year;
                        break;
                    case 7:
                        MesLabel = "Julio " + title.Year;
                        break;
                    case 8:
                        MesLabel = "Agosto " + title.Year;
                        break;
                    case 9:
                        MesLabel = "Septiembre " + title.Year;
                        break;
                    case 10:
                        MesLabel = "Octubre " + title.Year;
                        break;
                    case 11:
                        MesLabel = "Noviembre " + title.Year;
                        break;
                    case 12:
                        MesLabel = "Diciembre " + title.Year;
                        break;
                }
                return MesLabel;
            }
            void Inicializar()
            {
                progress = ProgressDialog.Show(this, "", "Obteniendo datos...", true, false);
                progress.SetProgressStyle(ProgressDialogStyle.Spinner);
                new Thread(new ThreadStart(delegate
                {
                    RunOnUiThread(async () => await CargarGrafico());
                })).Start();
            }

            async Task CargarGrafico()
            {
                List<BEGraficosFacturacion> resultado;
                List<TBLDistribucionFacturada> resultado1;
                DAReporte_Facturacion conexion = new DAReporte_Facturacion();
                BLTBLDistribucionFacturada conexion1 = new BLTBLDistribucionFacturada();
                TBLDistribucionFacturada data = new TBLDistribucionFacturada();
                string MensajeErrorDF;
                if (_modooff == "0")
                {
                    resultado = await conexion.Reporte_DistribucionFacturacion(_fecha, _codCliente, _codPuntoF, _moneda);
                    MensajeErrorDF = conexion.ResultadoFinalRDF;

                    if (resultado == null)
                    {
                        progress.Dismiss();
                        Toast.MakeText(this, MensajeErrorDF, ToastLength.Long).Show();
                    }
                    else if (resultado.Count == 0)
                    {
                        progress.Dismiss();
                        Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this);
                        alert.SetMessage("No hay datos.");
                        alert.SetCancelable(false);
                        alert.SetPositiveButton("OK", (senderAlert, args) =>
                        {
                            Finish();
                        });
                        alert.Show();
                    }
                    else
                    {
                        var modelAllocValues1 = resultado.Select(x => Convert.ToDouble(x.Consumo)).ToArray();
                        var modelAllocations1 = resultado.Select(x => x.Concepto).ToArray();
                        var total1 = resultado.Sum(x => Convert.ToDouble(x.Consumo));
                        GraficarPie(modelAllocValues1, modelAllocations1, total1);
                        TitleMes.Text = TitleMesAnio(_fecha);
                    }
                }
                else if (_modooff != "0")
                {
                    data.Mes = _fecha.Substring(3, 2);
                    data.Anio = _fecha.Substring(6, 4);
                    data.CodCliente = _codCliente;
                    data.CodPuntoFacturacion = _codPuntoF;
                    resultado1 = await conexion1.Reporte_DistribucionFacturacion(data);
                    MensajeErrorDF = "Error";
                    if (resultado1 == null)
                    {
                        progress.Dismiss();
                        Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this);
                        alert.SetTitle("Modo OffLine");
                        alert.SetMessage("No hay datos sincronizados.");
                        alert.SetCancelable(false);
                        alert.SetPositiveButton("OK", (senderAlert, args) =>
                        {
                            Finish();
                        });
                        alert.Show();
                    }
                    else if (resultado1.Count == 0)
                    {
                        resultado1 = await conexion1.Reporte_DistribucionFacturacionTodo(data);
                        if (resultado1 == null)
                        {
                            progress.Dismiss();
                            Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this);
                            alert.SetTitle("Modo OffLine");
                            alert.SetMessage("No hay datos sincronizados.");
                            alert.SetCancelable(false);
                            alert.SetPositiveButton("OK", (senderAlert, args) =>
                            {
                                Finish();
                            });
                            alert.Show();
                        }
                        else if (resultado1.Count == 0)
                        {
                            progress.Dismiss();
                            Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this);
                            alert.SetTitle("Modo OffLine");
                            alert.SetMessage("No hay datos sincronizados.");
                            alert.SetCancelable(false);
                            alert.SetPositiveButton("OK", (senderAlert, args) =>
                            {
                                Finish();
                            });
                            alert.Show();
                        }
                        else
                        {
                            string _meses = "";
                            if (_modooff == "1") { _meses = " mes."; } else { _meses = " meses."; }
                            Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this);
                            alert.SetTitle("Modo OffLine");
                            alert.SetMessage("Usted solo tiene datos sincronizados de " + _modooff + _meses);
                            alert.SetCancelable(false);
                            alert.SetPositiveButton("OK", (senderAlert, args) =>
                            {
                                var _ultimoMes = resultado1.First(x => (x.Fecha < DateTime.Now)).Fecha;
                                DateTime busqueda = new DateTime(_ultimoMes.Year, _ultimoMes.Month, 1);
                                var modelAllocValues1 = resultado1.Where(x => x.Fecha >= busqueda).OrderBy(x => x.Fecha).Select(x => Convert.ToDouble(x.Consumo)).ToArray();
                                var modelAllocations1 = resultado1.Where(x => x.Fecha >= busqueda).OrderBy(x => x.Fecha).Select(x => x.Concepto).ToArray();
                                var total1 = resultado1.Where(x => x.Fecha >= busqueda).OrderBy(x => x.Fecha).Sum(x => Convert.ToDouble(x.Consumo));
                                GraficarPie(modelAllocValues1, modelAllocations1, total1);
                                TitleMes.Text = TitleMesAnio(_ultimoMes.ToString("dd/MM/yyyy"));
                            });
                            alert.Show();
                        }
                    }
                    else
                    {
                        var modelAllocValues1 = resultado1.Select(x => Convert.ToDouble(x.Consumo)).ToArray();
                        var modelAllocations1 = resultado1.Select(x => x.Concepto).ToArray();
                        var total1 = resultado1.Sum(x => Convert.ToDouble(x.Consumo));
                        GraficarPie(modelAllocValues1, modelAllocations1, total1);
                        TitleMes.Text = TitleMesAnio(_fecha);
                    }
                }

                void GraficarPie(double[] modelAllocValues1, string[] modelAllocations1, double total1)
                {
                    try
                    {
                        for (int i = 0; i < modelAllocations1.Length && i < modelAllocValues1.Length && i < colors.Length; i++)
                        {
                            pieSeries2.Slices.Add(new PieSlice("", modelAllocValues1[i]) { Fill = OxyColor.Parse(colors[i]), IsExploded = true });

                            double mValue = modelAllocValues1[i];
                            double percentValue = (mValue / total1) * 100;
                            string percent = percentValue.ToString("N2");

                            //Add horizontal layout for titles and colors of slices
                            LinearLayout hLayot = new LinearLayout(this);
                            hLayot.Orientation = Android.Widget.Orientation.Horizontal;
                            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WrapContent, LinearLayout.LayoutParams.WrapContent);
                            hLayot.LayoutParameters = param;

                            //Add views with colors
                            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(40, 20);

                            //

                            TableRow.LayoutParams layoutCelda;
                            TableRow fila = new TableRow(this);
                            TableRow.LayoutParams layoutFila = new TableRow.LayoutParams(TableRow.LayoutParams.WrapContent, TableRow.LayoutParams.WrapContent);
                            fila.LayoutParameters = layoutFila;

                            for (var _i = 0; _i < 3; _i++)
                            {
                                if (_i == 0)
                                {
                                    //tableRow.AddView(mView);
                                    View mView = new View(this);
                                    mView.SetBackgroundColor(Android.Graphics.Color.ParseColor(colors[i]));
                                    layoutCelda = new TableRow.LayoutParams(40, 20);
                                    layoutCelda.Gravity = GravityFlags.Center;
                                    mView.LayoutParameters = layoutCelda;
                                    fila.AddView(mView);
                                }
                                if (_i == 1)
                                {
                                    //Negrita
                                    TextView porcentaje = new TextView(this);
                                    porcentaje.TextSize = 14;
                                    porcentaje.SetTextColor(Android.Graphics.Color.Black);
                                    porcentaje.SetTypeface(null, TypefaceStyle.Bold);
                                    porcentaje.Text = " " + percent + "% ";
                                    layoutCelda = new TableRow.LayoutParams(TableRow.LayoutParams.WrapContent, TableRow.LayoutParams.WrapContent);
                                    layoutCelda.Gravity = GravityFlags.Right;
                                    porcentaje.LayoutParameters = layoutCelda;
                                    fila.AddView(porcentaje);
                                }
                                if (_i == 2)
                                {
                                    TextView concepto = new TextView(this);
                                    concepto.TextSize = 14;
                                    concepto.SetTextColor(Android.Graphics.Color.Black);
                                    var consumo = mValue.ToString("N2");
                                    concepto.Text = modelAllocations1[i] + " ($ " + consumo + ")";
                                    layoutCelda = new TableRow.LayoutParams(TableRow.LayoutParams.WrapContent, TableRow.LayoutParams.WrapContent);
                                    concepto.LayoutParameters = layoutCelda;
                                    fila.AddView(concepto);
                                }
                            }
                            mLLayoutModel.AddView(fila);
                        }
                        plotModel2.Series.Add(pieSeries2);
                        MyModel = plotModel2;
                        plotViewModel.Model = MyModel;
                        progress.Dismiss();
                    }
                    catch (Exception ex)
                    {
                        Toast.MakeText(this, ex.Message, ToastLength.Short).Show();
                    }
                }
            }
        }

        //BackButton
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home)
            {
                Finish();
            }
            return base.OnOptionsItemSelected(item);
        }
    }
}