﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;
using ML;

namespace E7.Droid.Facturacion
{
    [Activity(Label = "PreFacActivity", Theme = "@style/Theme.DesignDemo", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class PreFacturacion : Fragment
    {
        DrawerLayout drawerLayout;
        NavigationView navigationView;
        Button btn1, btn2, btn3;
        Android.App.ProgressDialog progress;
        string _nombreCompleto, _filtro;

        public PreFacturacion(string nombre, string filtro)
        {
            _nombreCompleto = nombre;
            _filtro = filtro;
        }
        public event EventHandler CallDistribucionFacturada;

        public void OnCallDistribucionFacturada()
        {
            EventHandler handler = CallDistribucionFacturada;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        public event EventHandler CallGraficoHistoricoFacturado;

        public void OnCallGraficoHistoricoFacturado()
        {
            EventHandler handler = CallGraficoHistoricoFacturado;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        public event EventHandler CallGraficoHistoricoPrecios;

        public void OnCallGraficoHistoricoPrecios()
        {
            EventHandler handler = CallGraficoHistoricoPrecios;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        public override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            return inflater.Inflate(Resource.Layout.PreFacturacion, container, false);
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);

            btn1 = View.FindViewById<Button>(Resource.Id.button1);
            btn2 = View.FindViewById<Button>(Resource.Id.button2);
            btn3 = View.FindViewById<Button>(Resource.Id.button3);
            btn1.Click += (sender, e) => fac1();
            btn2.Click += (sender, e) => fac2();
            btn3.Click += (sender, e) => fac3();

            void fac1()
            {
                OnCallDistribucionFacturada();
            }
            void fac2()
            {
                OnCallGraficoHistoricoFacturado();
            }
            void fac3()
            {
                OnCallGraficoHistoricoPrecios();
            }
        }
    }
}