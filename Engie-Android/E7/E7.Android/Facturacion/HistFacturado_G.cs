﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;
using SciChart.Charting.Visuals;
using SciChart.Charting.Visuals.Axes;
using SciChart.Charting.Model.DataSeries;
using SciChart.Charting.Visuals.RenderableSeries;
using SciChart.Charting.Modifiers;
using SciChart.Drawing.Common;
using System.Threading.Tasks;
using System.Threading;
using SciChart.Data.Model;
using SciChart.Charting.Visuals.Legend;
using E7.Droid.Database;
using System.Globalization;
using SciChart.Core.Utility;
using E7.Servicio;
using E7.Entidades;
using E7.Droid.Utiles;
using E7.Model;
using ML;

namespace E7.Droid.Facturacion
{
    [Activity(Label = "", Icon = "@drawable/calendario", Theme = "@style/Theme.DesignDemo", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class HistFacturado_G : AppCompatActivity
    {
        DrawerLayout drawerLayout;
        NavigationView navigationView;
        string _usuarioInterno, filtro;
        Android.App.ProgressDialog progress;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.HistFacturado_G);
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(GetDrawable(Resource.Drawable.backButton));

            TextView ToolOff = FindViewById<TextView>(Resource.Id.modooff);
            //var chart = FindViewById<SciChartSurface>(Resource.Id.Chart);
            //var _legend = FindViewById<SciChartSurface>(Resource.Id.Chart);
            var chart = new SciChartSurface(this);
            var linearLayoutC = FindViewById<LinearLayout>(Resource.Id.linearLayout1);
            var linearLayoutL = FindViewById<LinearLayout>(Resource.Id.linearLayout2);
            int themeid = Resource.Style.SciChart_Bright_Spark;
            chart.Theme = themeid;

            _usuarioInterno = Intent.GetStringExtra("_usuarioInterno") ?? "Data not available";
            filtro = Intent.GetStringExtra("filtro") ?? "Data not available";
            string _codCliente = Intent.GetStringExtra("_codCliente") ?? "Data not available";
            string _codPuntoF = Intent.GetStringExtra("_codPuntoF") ?? "Data not available";
            string _moneda = Intent.GetStringExtra("_moneda") ?? "Data not available";
            string _fechaD = Intent.GetStringExtra("_fechaD") ?? "Data not available";
            string _fechaH = Intent.GetStringExtra("_fechaH") ?? "Data not available";
            string _modooff = Intent.GetStringExtra("_modooff") ?? "0";

            ModoOff();
            Inicializar();
            void ModoOff()
            {
                if (_modooff == "0")
                {
                    ToolOff.Visibility = ViewStates.Gone;
                }
                else
                {
                    ToolOff.Visibility = ViewStates.Visible;
                }
            }
            void Inicializar()
            {
                progress = ProgressDialog.Show(this, "", "Obteniendo datos...", true, false);
                progress.SetProgressStyle(ProgressDialogStyle.Spinner);
                new Thread(new ThreadStart(delegate
                {
                    RunOnUiThread(async () => await CargarGrafico());
                })).Start();
            }

            async Task CargarGrafico()
            {
                List<BEGraficosFacturacion> resultado;
                List<TBLHistoricoFacturado> resultado1;
                DAReporte_Facturacion conexion = new DAReporte_Facturacion();
                BLTBLHistoricoFacturado conexion1 = new BLTBLHistoricoFacturado();
                TBLHistoricoFacturado data = new TBLHistoricoFacturado();

                string mhp;
                string MensajeErrorHF;

                if (_moneda == "USD") { mhp = "$ "; } else if (_moneda == "PEN") { mhp = "S/ "; } else { mhp = ""; }
                if (_modooff == "0")
                {
                    resultado = await conexion.Reporte_HistoricoFacturado(_fechaD, _fechaH, _codCliente, _codPuntoF, _moneda);
                    MensajeErrorHF = conexion.ResultadoFinalRHF;
                    if (resultado == null)
                    {
                        progress.Dismiss();
                        Toast.MakeText(this, MensajeErrorHF, ToastLength.Short).Show();
                    }
                    else if (resultado.Count == 0)
                    {
                        progress.Dismiss();
                        Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this);
                        alert.SetMessage("No hay datos.");
                        alert.SetCancelable(false);
                        alert.SetPositiveButton("OK", (senderAlert, args) =>
                        {
                            Finish();
                        });
                        alert.Show();
                    }
                    else
                    {
                        var Fecha = resultado.OrderBy(x => x.Fecha).Select(x => x.Fecha).Distinct().ToArray();
                        var EnergiaActiva = resultado.OrderBy(x => x.Fecha).Where(x => x.Concepto == "EnergiaActiva").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                        var Potencia = resultado.OrderBy(x => x.Fecha).Where(x => x.Concepto == "Potencia").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                        var EnergiaReactiva = resultado.OrderBy(x => x.Fecha).Where(x => x.Concepto == "EnergiaReactiva").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                        var PeajePrincipal = resultado.OrderBy(x => x.Fecha).Where(x => x.Concepto == "PeajePrincipal").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                        var PeajeSecundario = resultado.OrderBy(x => x.Fecha).Where(x => x.Concepto == "PeajeSecundario").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                        var Otros = resultado.OrderBy(x => x.Fecha).Where(x => x.Concepto == "Otros").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                        var ElectrificacionRural = resultado.OrderBy(x => x.Fecha).Where(x => x.Concepto == "ElectrificacionRural").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                        var FISE = resultado.OrderBy(x => x.Fecha).Where(x => x.Concepto == "FISE").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                        var cont = (Convert.ToDouble(resultado.Count) / Convert.ToDouble(8));

                        GraficarColumn(Fecha, EnergiaActiva, EnergiaReactiva, PeajePrincipal, PeajeSecundario, Otros, FISE, Potencia, ElectrificacionRural, cont);
                    }
                }
                else if (_modooff != "0")
                {
                    DateTime desde = DateTime.ParseExact(_fechaD, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    DateTime _desde = new DateTime(desde.Year, desde.Month, 1);
                    DateTime _Fdesde = new DateTime(desde.Year, desde.Month, 20);
                    DateTime hasta = DateTime.ParseExact(_fechaH, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    DateTime _hasta = new DateTime(hasta.Year, hasta.Month, 20);
                    data.CodCliente = _codCliente;
                    data.CodPuntoFacturacion = _codPuntoF;
                    data.Moneda = _moneda;
                    resultado1 = await conexion1.Reporte_HistoricoFacturado(data, _desde, _hasta);
                    var _ultimoMes = resultado1.Where(x => (x.Fecha <= _Fdesde)).ToArray();
                    MensajeErrorHF = "Error al momento de leer los datos guardados.";
                    if (resultado1 == null)
                    {
                        progress.Dismiss();
                        Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this);
                        alert.SetTitle("Modo OffLine");
                        alert.SetMessage("No hay datos sincronizados.");
                        alert.SetCancelable(false);
                        alert.SetPositiveButton("OK", (senderAlert, args) =>
                        {
                            Finish();
                        });
                        alert.Show();
                    }
                    else if (resultado1.Count == 0 || _ultimoMes.Count() == 0)
                    {
                        resultado1 = await conexion1.Reporte_HistoricoFacturado(data);

                        if (resultado1 == null)
                        {
                            progress.Dismiss();
                            Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this);
                            alert.SetTitle("Modo OffLine");
                            alert.SetMessage("No hay datos sincronizados");
                            alert.SetCancelable(false);
                            alert.SetPositiveButton("OK", (senderAlert, args) =>
                            {
                                Finish();
                            });
                            alert.Show();
                        }
                        else if (resultado1.Count == 0)
                        {
                            progress.Dismiss();
                            Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this);
                            alert.SetTitle("Modo OffLine");
                            alert.SetMessage("No hay datos sincronizados.");
                            alert.SetCancelable(false);
                            alert.SetPositiveButton("OK", (senderAlert, args) =>
                            {
                                Finish();
                            });
                            alert.Show();
                        }
                        else
                        {
                            string _meses = "";
                            if (_modooff == "1") { _meses = " mes."; } else { _meses = " meses."; }
                            Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this);
                            alert.SetTitle("Modo OffLine");
                            alert.SetMessage("Usted solo tiene datos sincronizados de " + _modooff + _meses);
                            alert.SetCancelable(false);
                            alert.SetPositiveButton("OK", (senderAlert, args) =>
                            {
                                var Fecha = resultado1.OrderBy(x => x.Fecha).Select(x => x.Fecha).Distinct().ToArray();
                                var EnergiaActiva = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "EnergiaActiva").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                                var Potencia = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "Potencia").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                                var EnergiaReactiva = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "EnergiaReactiva").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                                var PeajePrincipal = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "PeajePrincipal").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                                var PeajeSecundario = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "PeajeSecundario").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                                var Otros = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "Otros").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                                var ElectrificacionRural = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "ElectrificacionRural").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                                var FISE = resultado1.OrderBy(x => x.Fecha).Where(x => x.Concepto == "FISE").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                                var cont = (Convert.ToDouble(resultado1.Count) / Convert.ToDouble(8));
                                GraficarColumn(Fecha, EnergiaActiva, EnergiaReactiva, PeajePrincipal, PeajeSecundario, Otros, FISE, Potencia, ElectrificacionRural, cont);
                            });
                            alert.Show();
                        }
                    }
                    else
                    {
                        var Fecha = resultado1.OrderBy(y => y.Fecha).Select(x => x.Fecha).Distinct().ToArray();
                        var EnergiaActiva = resultado1.OrderBy(y => y.Fecha).Where(x => x.Concepto == "EnergiaActiva").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                        var Potencia = resultado1.OrderBy(y => y.Fecha).Where(x => x.Concepto == "Potencia").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                        var EnergiaReactiva = resultado1.OrderBy(y => y.Fecha).Where(x => x.Concepto == "EnergiaReactiva").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                        var PeajePrincipal = resultado1.OrderBy(y => y.Fecha).Where(x => x.Concepto == "PeajePrincipal").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                        var PeajeSecundario = resultado1.OrderBy(y => y.Fecha).Where(x => x.Concepto == "PeajeSecundario").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                        var Otros = resultado1.OrderBy(y => y.Fecha).Where(x => x.Concepto == "Otros").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                        var ElectrificacionRural = resultado1.OrderBy(y => y.Fecha).Where(x => x.Concepto == "ElectrificacionRural").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                        var FISE = resultado1.OrderBy(y => y.Fecha).Where(x => x.Concepto == "FISE").Select(x => (Convert.ToDouble(x.Consumo))).ToArray();
                        var cont = (Convert.ToDouble(resultado1.Count) / Convert.ToDouble(8));

                        GraficarColumn(Fecha, EnergiaActiva, EnergiaReactiva, PeajePrincipal, PeajeSecundario, Otros, FISE, Potencia, ElectrificacionRural, cont);
                    }
                }

                void GraficarColumn(DateTime[] Fecha, double[] EnergiaActiva, double[] EnergiaReactiva, double[] PeajePrincipal,
                    double[] PeajeSecundario, double[] Otros, double[] FISE, double[] Potencia, double[] ElectrificacionRural, double cont)
                {
                    try
                    {
                        var xAxis = new NumericAxis(this)
                        {
                            GrowBy = new DoubleRange(0, 0),
                            DrawMajorGridLines = false,
                            DrawMinorGridLines = false,
                            LabelProvider = new MonthYearLabelProvider(),
                            AxisBandsStyle = new SolidBrushStyle(0xfff9f9f9),
                        };
                        var yAxis = new NumericAxis(this)
                        {
                            AxisTitle = "Total " + _moneda,
                            LabelProvider = new MillionsLabelProvider(),
                            GrowBy = new DoubleRange(0, 0.1d),
                            CursorTextFormatting = mhp + "##,##0.00",
                            AxisAlignment = AxisAlignment.Left,
                            DrawMajorGridLines = false,
                            DrawMinorGridLines = false,
                            AxisBandsStyle = new SolidBrushStyle(0xfff9f9f9),
                        };

                        var EnergiaActivaDataSeries = new XyDataSeries<double, double> { SeriesName = "Energia Activa" };
                        var PotenciaDataSeries = new XyDataSeries<double, double> { SeriesName = "Potencia" };
                        var EnergiaReactivaDataSeries = new XyDataSeries<double, double> { SeriesName = "Energia Reactiva" };
                        var PeajePrincipalDataSeries = new XyDataSeries<double, double> { SeriesName = "Peaje Principal" };
                        var PeajeSecundarioDataSeries = new XyDataSeries<double, double> { SeriesName = "Peaje Secundario" };
                        var OtrosDataSeries = new XyDataSeries<double, double> { SeriesName = "Otros" };
                        var ElectrificacionRuralDataSeries = new XyDataSeries<double, double> { SeriesName = "Electrificacion Rural" };
                        var FISEDataSeries = new XyDataSeries<double, double> { SeriesName = "FISE" };
                        var totalDataSeries = new XyDataSeries<double, double> { SeriesName = "Total" };
                        //DateTime _fecha;
                        for (var i = 0; i < cont; i++)
                        {
                            string dia = "";
                            if (i.ToString().Length == 1)
                            {
                                dia = "0" + i;
                            }
                            else
                            {
                                dia = i.ToString();
                            }
                            string anio = Fecha[0].ToString("yyyy");
                            string mes = Fecha[0].ToString("MM");
                            double AnioMes = Convert.ToDouble(string.Format("{0}{1}{2}", anio, mes, dia));
                            EnergiaActivaDataSeries.Append(AnioMes, EnergiaActiva[i]);
                            PotenciaDataSeries.Append(AnioMes, Potencia[i]);
                            EnergiaReactivaDataSeries.Append(AnioMes, EnergiaReactiva[i]);
                            PeajePrincipalDataSeries.Append(AnioMes, PeajePrincipal[i]);
                            PeajeSecundarioDataSeries.Append(AnioMes, PeajeSecundario[i]);
                            OtrosDataSeries.Append(AnioMes, Otros[i]);
                            ElectrificacionRuralDataSeries.Append(AnioMes, ElectrificacionRural[i]);
                            FISEDataSeries.Append(AnioMes, FISE[i]);
                            totalDataSeries.Append(AnioMes, EnergiaActiva[i] + Potencia[i] + EnergiaReactiva[i] + PeajePrincipal[i] + PeajeSecundario[i] + Otros[i] + ElectrificacionRural[i] + FISE[i]);
                        }

                        var columnsCollection = new HorizontallyStackedColumnsCollection();

                        columnsCollection.Add(GetRenderableSeries(EnergiaActivaDataSeries, 0xff7cb5ec, 0xff7cb5ec, true));
                        columnsCollection.Add(GetRenderableSeries(PotenciaDataSeries, 0xFF434348, 0xFF434348, true));
                        columnsCollection.Add(GetRenderableSeries(EnergiaReactivaDataSeries, 0xFF90ed7d, 0xFF90ed7d, true));
                        columnsCollection.Add(GetRenderableSeries(PeajePrincipalDataSeries, 0xFFf7a35c, 0xFFf7a35c, true));
                        columnsCollection.Add(GetRenderableSeries(PeajeSecundarioDataSeries, 0xFF8085e9, 0xFF8085e9, true));
                        columnsCollection.Add(GetRenderableSeries(OtrosDataSeries, 0xfff15c80, 0xfff15c80, true));
                        columnsCollection.Add(GetRenderableSeries(ElectrificacionRuralDataSeries, 0xFFe4d354, 0xFFe4d354, true));
                        columnsCollection.Add(GetRenderableSeries(FISEDataSeries, 0xff2b908f, 0xff2b908f, true));
                        columnsCollection.Add(GetRenderableSeries(totalDataSeries, 0xff005fa3, 0xff005fa3, false));

                        var legend = new SciChartLegend(this);

                        var legendModifier = new LegendModifier(legend);
                        legendModifier.SetOrientation(SciChart.Core.Framework.Orientation.Vertical);
                        legendModifier.SetLegendPosition(GravityFlags.Top | GravityFlags.Start, 0);
                        legendModifier.SetSourceMode(SourceMode.AllVisibleSeries);
                        legendModifier.SetShowSeriesMarkers(true);
                        legendModifier.SetShowCheckboxes(true);
                        legendModifier.SetReceiveHandledEvents(true);


                        var modifierGroup = new ModifierGroup(legendModifier);

                        chart.ChartModifiers.Add(modifierGroup);
                        if (legend.Parent != null)
                        {
                            (legend.Parent as ViewGroup).RemoveView(legend);
                        }

                        chart.XAxes.Add(xAxis);
                        chart.YAxes.Add(yAxis);
                        chart.RenderableSeries.Add(columnsCollection);
                        chart.ChartModifiers.Add(new TooltipModifier());
                        //Zoom
                        chart.ChartModifiers.Add(new PinchZoomModifier
                        {
                            Direction = SciChart.Charting.Direction2D.XDirection,
                            ScaleFactor = 0.4f,
                            IsUniformZoom = true,
                        });
                        //Volver al inicio
                        chart.ChartModifiers.Add(new ZoomExtentsModifier());
                        //Mover
                        chart.ChartModifiers.Add(new ZoomPanModifier
                        {
                            Direction = SciChart.Charting.Direction2D.XDirection,
                            ZoomExtentsY = false,
                            ClipModeX = SciChart.Charting.ClipMode.ClipAtExtents
                        });
                        linearLayoutC.Orientation = Orientation.Vertical;
                        linearLayoutL.Orientation = Orientation.Vertical;
                        linearLayoutC.AddView(chart);
                        linearLayoutL.AddView(legend);
                        progress.Dismiss();
                    }
                    catch (Exception ex)
                    {
                        progress.Dismiss();
                        Toast.MakeText(this, ex.Message, ToastLength.Short).Show();
                    }
                }
            }
        }
        private StackedColumnRenderableSeries GetRenderableSeries(IDataSeries dataSeries, uint fillColor, uint strokeColor, bool visible)
        {
            return new StackedColumnRenderableSeries
            {
                DataSeries = dataSeries,
                StrokeStyle = new SolidPenStyle(this, strokeColor),
                FillBrushStyle = new SolidBrushStyle(fillColor),
                IsVisible = visible,
            };
        }

        //BackButton
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home)
            {
                Finish();
            }
            return base.OnOptionsItemSelected(item);
        }
    }
}
