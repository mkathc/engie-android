﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;
using System.Threading;
using System.Threading.Tasks;
using E7.Droid.Database;
using E7.Entidades;
using E7.Servicio;
using E7.Droid.Utiles;
using ML;
using E7.Util;

namespace E7.Droid.Facturacion
{
    [Activity(Label = "", Theme = "@style/Theme.DesignDemo", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class DistFacturacion_F : AppCompatActivity
    {
        DrawerLayout drawerLayout;
        NavigationView navigationView;
        ClientesAdapter adapter_c;
        PuntoFacturacionAdapter adapter_p;
        MesAdapter adapter_m;
        JavaList<BEClientes> clientes;
        JavaList<BEPuntoFacturacion> puntoFacturacion;
        JavaList<Mes> mes;
        Button Graficar;
        Spinner CodCliente, IdPuntoFacturacion, mesD, anioD;
        string _codCliente, _codPuntoF, _anioD, _mesD, _fecha, filtro, _usuarioInterno, _modooff;
        DateTime date;
        int _array;
        string[] items;
        DAReporte_Facturacion conexion = new DAReporte_Facturacion();
        DATBLDistribucionFacturada DAdf = new DATBLDistribucionFacturada();
        UTComunes util = new UTComunes();
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Create your application here

            //Listar Clientes y Punto de Facturacion
            _usuarioInterno = Intent.GetStringExtra("_usuarioInterno") ?? "Data not available";
            filtro = Intent.GetStringExtra("filtro") ?? "Data not available";
            ISharedPreferences pref = Application.Context.GetSharedPreferences("UserInfo", FileCreationMode.Private);
            _modooff = pref.GetString("_modooff", "0");

            clientes = (JavaList<BEClientes>)Newtonsoft.Json.JsonConvert.DeserializeObject<JavaList<BEClientes>>(filtro);

            SetContentView(layoutResID: Resource.Layout.DistFacturacion_F);
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(GetDrawable(Resource.Drawable.backButton));
            drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);
            Graficar = FindViewById<Button>(Resource.Id.button1);
            //Filtros

            CodCliente = FindViewById<Spinner>(Resource.Id.spinner1);
            IdPuntoFacturacion = FindViewById<Spinner>(Resource.Id.spinner2);
            mesD = FindViewById<Spinner>(Resource.Id.spinner3);
            anioD = FindViewById<Spinner>(Resource.Id.spinner4);

            CodCliente.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(CodCliente_ItemSelected);
            IdPuntoFacturacion.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(IdPuntoFacturacion_ItemSelected);
            mesD.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(MesD_ItemSelected);
            anioD.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(AnioD_ItemSelected);

            ListarClientes();
            _Anio();
            ListarMesD();
            ListarAnioD();

            Graficar.Click += (sender, e) => Grafic();

            void Grafic()
            {
                if (!string.IsNullOrEmpty(_codCliente) && !string.IsNullOrEmpty(_codPuntoF) &&
                    !string.IsNullOrEmpty(_mesD) && !string.IsNullOrEmpty(_anioD))
                {
                    //progress = ProgressDialog.Show(this, "", "Cargando...", true, false);
                    //progress.SetProgressStyle(ProgressDialogStyle.Spinner);
                    new Thread(new ThreadStart(delegate
                    {
                        RunOnUiThread(async () => await Graficos());
                    })).Start();
                }
                else if (string.IsNullOrEmpty(_codCliente))
                {
                    string toast = "Seleccione un cliente";
                    Toast.MakeText(this, toast, ToastLength.Short).Show();

                }
                else if (string.IsNullOrEmpty(_codPuntoF))
                {
                    string toast = "Seleccione un punto de facturación";
                    Toast.MakeText(this, toast, ToastLength.Short).Show();
                }
                else if (string.IsNullOrEmpty(_mesD))
                {
                    string toast = "Seleccione el mes";
                    Toast.MakeText(this, toast, ToastLength.Short).Show();
                }
                else if (string.IsNullOrEmpty(_anioD))
                {
                    string toast = "Seleccione el año";
                    Toast.MakeText(this, toast, ToastLength.Short).Show();
                }
            }

            async Task Graficos()
            {
                Graficar.Enabled = false;
                _fecha = string.Format("01/{0}/{1}", _mesD, _anioD);
                var IsConnected = await util.IsConnected();

                if (IsConnected == true)
                {
                    var intent_data = new Intent(this, typeof(piechart));
                    intent_data.PutExtra("_usuarioInterno", _usuarioInterno);
                    intent_data.PutExtra("filtro", filtro);
                    intent_data.PutExtra("_codCliente", _codCliente);
                    intent_data.PutExtra("_codPuntoF", _codPuntoF);
                    intent_data.PutExtra("_fecha", _fecha);
                    intent_data.PutExtra("_modooff", "0");
                    StartActivity(intent_data);
                    Graficar.Enabled = true;
                    //progress.Dismiss();
                }
                else if (IsConnected == false)
                {
                    if (_modooff != "0")
                    {
                        var intent_data = new Intent(this, typeof(piechart));
                        intent_data.PutExtra("_usuarioInterno", _usuarioInterno);
                        intent_data.PutExtra("filtro", filtro);
                        intent_data.PutExtra("_codCliente", _codCliente);
                        intent_data.PutExtra("_codPuntoF", _codPuntoF);
                        intent_data.PutExtra("_fecha", _fecha);
                        intent_data.PutExtra("_modooff", _modooff);
                        StartActivity(intent_data);
                        Graficar.Enabled = true;
                        //progress.Dismiss();
                    }
                    else
                    {
                        Toast.MakeText(this, "En estos momentos no tiene servicio de internet ni datos sincronizados en Modo OffLine, por favor verificar.", ToastLength.Long).Show();
                        Graficar.Enabled = true;
                        //progress.Dismiss();
                    }
                }
            }

            void ListarClientes()
            {
                _codPuntoF = null;
                var _clientes = clientes;
                adapter_c = new ClientesAdapter(this, clientes);
                CodCliente.Adapter = adapter_c;
            }

            void CodCliente_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                _codCliente = clientes[e.Position].CodCliente;
                ListarPuntoFacturacion(_codCliente);
            }
            void ListarPuntoFacturacion(string _codCliente)
            {
                var _puntoFacturacion = clientes.Where(x => x.CodCliente == _codCliente).Select(x => x.PFaCliente).ToList();
                foreach (JavaList<BEPuntoFacturacion> item in _puntoFacturacion)
                {
                    puntoFacturacion = item;
                }
                adapter_p = new PuntoFacturacionAdapter(this, puntoFacturacion);
                IdPuntoFacturacion.Adapter = adapter_p;
            }
            void IdPuntoFacturacion_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                _codPuntoF = puntoFacturacion[e.Position].ID;
            }
            void ListarMesD()
            {
                mes = COMes.ListarMes();
                adapter_m = new MesAdapter(this, mes);
                mesD.Adapter = adapter_m;
                var i = ((DateTime.Now).Month) - 1;
                mesD.SetSelection(i);
            }

            void MesD_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                _mesD = mes[e.Position].IdMes;
            }
            void _Anio()
            {
                date = DateTime.Now;
                _array = date.Year - 2014;
                items = new string[_array];
                string anio = null;
                int _i = 0;
                for (int i = 2015; i <= date.Year; i++)
                {
                    anio = i.ToString();
                    items[_i] = anio;
                    _i = _i + 1;
                }
            }
            void ListarAnioD()
            {
                var Adaptador = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, items);
                anioD.Adapter = Adaptador;
                anioD.SetSelection(_array - 1);
            }
            void AnioD_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
            {
                Spinner spinner = (Spinner)sender;
                _anioD = spinner.GetItemAtPosition(e.Position).ToString();
            }
        }
        //BackButton
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home) ;
            {
                Finish();
            }
            return base.OnOptionsItemSelected(item);
        }
    }
}

