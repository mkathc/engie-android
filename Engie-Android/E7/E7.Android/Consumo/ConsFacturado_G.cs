﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Android.Support.V4.View;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;
using System.Threading;
using System.Threading.Tasks;
using E7.Entidades;
using E7.Servicio;
using E7.Droid.Database;
using E7.Droid.Utiles;
using E7.Model;
using ML;

namespace E7.Droid.Consumo
{
    [Activity(Label = "", Theme = "@style/Theme.DesignDemo", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class ConsFacturado_G : AppCompatActivity
    {
        //DrawerLayout drawerLayout;
        //NavigationView navigationView;
        string filtro, _anio, _codCliente, _codPuntoF, _modooff, _nombreCompleto;
        //Thread thread = null;
        Android.App.ProgressDialog progress;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.ConsFacturado_G);
            var toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(GetDrawable(Resource.Drawable.backButton));

            // Create your application here

            TabLayout TabLayout = FindViewById<TabLayout>(Resource.Id.tabLayout);
            ViewPager ViewPager = FindViewById<ViewPager>(Resource.Id.viewpager);
            TextView ToolOff = FindViewById<TextView>(Resource.Id.modooff);

            IList<ChartTypeModel> _chartTypesSource = new List<ChartTypeModel>();

            //Cargar Datos
            _nombreCompleto = Intent.GetStringExtra("_nombreCompleto") ?? "Data not available";
            _anio = Intent.GetStringExtra("_anio") ?? "Data not available";
            _codCliente = Intent.GetStringExtra("_codCliente") ?? "Data not available";
            _codPuntoF = Intent.GetStringExtra("_codPuntoF") ?? "Data not available";
            filtro = Intent.GetStringExtra("filtro") ?? "Data not available";
            _modooff = Intent.GetStringExtra("_modooff") ?? "0";

            ModoOff();
            Inicializar();
            void ModoOff()
            {
                if (_modooff == "0")
                {
                    ToolOff.Visibility = ViewStates.Gone;
                }
                else
                {
                    ToolOff.Visibility = ViewStates.Visible;
                }
            }
            void Inicializar()
            {
                progress = ProgressDialog.Show(this, "", "Obteniendo datos...", true, false);
                progress.SetProgressStyle(ProgressDialogStyle.Spinner);
                new Thread(new ThreadStart(delegate
                {
                    RunOnUiThread(async () => await CargarGrafico());

                })).Start();

            }

            async Task CargarGrafico()
            {
                List<BEConsumoFacturado> resultado;
                List<TBLConsumoFacturado> resultado1;
                DAReporte_Consumo conexion = new DAReporte_Consumo();
                BLTBLConsumoFacturado conexion1 = new BLTBLConsumoFacturado();
                TBLConsumoFacturado data = new TBLConsumoFacturado();
                string MensajeErrorCF;
                if (_modooff == "0")
                {
                    resultado = await conexion.Reporte_ConsumoFacturado(_anio, _codCliente, _codPuntoF);
                    MensajeErrorCF = conexion.ResultadoFinalRCF;
                    if (resultado == null)
                    {
                        progress.Dismiss();
                        Toast.MakeText(this, MensajeErrorCF, ToastLength.Short).Show();
                    }
                    else if (resultado.Count == 0)
                    {
                        progress.Dismiss();
                        Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this);
                        alert.SetMessage("No hay datos.");
                        alert.SetCancelable(false);
                        alert.SetPositiveButton("OK", (senderAlert, args) =>
                        {
                            Finish();
                        });
                        alert.Show();
                    }
                    else
                    {
                        var EnergiaActiva = resultado.OrderBy(y => y.Fecha).Select(x => (Convert.ToDouble(x.EnergiaActiva))).ToArray();
                        var Potencia = resultado.OrderBy(y => y.Fecha).Select(x => (Convert.ToDouble(x.Potencia))).ToArray();
                        var Fecha = resultado.OrderBy(y => y.Fecha).Select(x => (Convert.ToDouble(x.Fecha.Month))).ToArray();

                        GraficarColumn(EnergiaActiva, Potencia, Fecha);
                    }
                }
                else if (_modooff != "0")
                {
                    data.Anio = _anio;
                    data.CodCliente = _codCliente;
                    data.CodPuntoFacturacion = _codPuntoF;
                    resultado1 = await conexion1.Reporte_ConsumoFacturado(data);
                    MensajeErrorCF = "Error al momento de leer los datos guardados";
                    if (resultado1 == null)
                    {
                        progress.Dismiss();
                        Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this);
                        alert.SetMessage("No hay datos sincronizados.");
                        alert.SetCancelable(false);
                        alert.SetPositiveButton("OK", (senderAlert, args) =>
                        {
                            Finish();
                        });
                        alert.Show();
                    }
                    else if (resultado1.Count == 0)
                    {
                        resultado1 = await conexion1.Reporte_ConsumoFacturadoTodo(data);
                        if (resultado1 == null)
                        {
                            progress.Dismiss();
                            Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this);
                            alert.SetTitle("Modo OffLine");
                            alert.SetMessage("No hay datos sincronizados.");
                            alert.SetCancelable(false);
                            alert.SetPositiveButton("OK", (senderAlert, args) =>
                            {
                                Finish();
                            });
                            alert.Show();
                        }
                        else if (resultado1.Count == 0)
                        {
                            progress.Dismiss();
                            Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this);
                            alert.SetTitle("Modo OffLine");
                            alert.SetMessage("No hay datos sincronizados.");
                            alert.SetCancelable(false);
                            alert.SetPositiveButton("OK", (senderAlert, args) =>
                            {
                                Finish();
                            });
                            alert.Show();
                        }
                        else
                        {
                            string _meses = "";
                            if (_modooff == "1") { _meses = " mes."; } else { _meses = " meses."; }
                            Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this);
                            alert.SetTitle("Modo OffLine");
                            alert.SetMessage("Usted solo tiene datos sincronizados de " + _modooff + _meses);
                            alert.SetCancelable(false);
                            alert.SetPositiveButton("OK", (senderAlert, args) =>
                            {
                                DateTime ahora = DateTime.Now;
                                var _ultimoMes = resultado1.OrderByDescending(x => x.Fecha).First(x => (x.Fecha < ahora.AddMonths(1))).Fecha;
                                DateTime busqueda = _ultimoMes.AddMonths(-(Convert.ToInt32(_modooff)));
                                DateTime _busqueda = new DateTime(busqueda.Year, busqueda.Month, 1);

                                var EnergiaActiva = resultado1.Where(x => x.Fecha >= _busqueda).OrderBy(y => y.Fecha).Select(x => (Convert.ToDouble(x.EnergiaActiva))).ToArray();
                                var Potencia = resultado1.Where(x => x.Fecha >= _busqueda).OrderBy(y => y.Fecha).Select(x => (Convert.ToDouble(x.Potencia))).ToArray();
                                var Fecha = resultado1.Where(x => x.Fecha >= _busqueda).OrderBy(y => y.Fecha).Select(x => (Convert.ToDouble(x.Fecha.Month))).ToArray();
                                GraficarColumn(EnergiaActiva, Potencia, Fecha);
                            });
                            alert.Show();
                        }
                    }
                    else
                    {
                        string _meses = "";
                        if (_modooff == "1") { _meses = " mes."; } else { _meses = " meses."; }
                        Android.App.AlertDialog.Builder alert = new Android.App.AlertDialog.Builder(this);
                        alert.SetTitle("Modo OffLine");
                        alert.SetMessage("Usted solo tiene datos sincronizados de " + _modooff + _meses);
                        alert.SetCancelable(false);
                        alert.SetPositiveButton("OK", (senderAlert, args) =>
                        {
                            DateTime ahora = DateTime.Now;
                        var _ultimoMes = resultado1.OrderByDescending(x => x.Fecha).First(x => (x.Fecha < ahora.AddMonths(1))).Fecha;
                        DateTime busqueda = _ultimoMes.AddMonths(-(Convert.ToInt32(_modooff)));
                        DateTime _busqueda = new DateTime(busqueda.Year, busqueda.Month, 1);

                        var EnergiaActiva = resultado1.Where(x => x.Fecha >= _busqueda).OrderBy(y => y.Fecha).Select(x => (Convert.ToDouble(x.EnergiaActiva))).ToArray();
                        var Potencia = resultado1.Where(x => x.Fecha >= _busqueda).OrderBy(y => y.Fecha).Select(x => (Convert.ToDouble(x.Potencia))).ToArray();
                        var Fecha = resultado1.Where(x => x.Fecha >= _busqueda).OrderBy(y => y.Fecha).Select(x => (Convert.ToDouble(x.Fecha.Month))).ToArray();
                        GraficarColumn(EnergiaActiva, Potencia, Fecha);
                        });
                        alert.Show();
                    }

                }
                void GraficarColumn(double[] EnergiaActiva, double[] Potencia, double[] Fecha)
                {
                    try
                    {
                        _chartTypesSource.Add(ChartTypeModelFactory.EnergiaActivaStackedColumn(EnergiaActiva, Fecha, 0xff7cb5ec, 0xff7cb5ec));
                        _chartTypesSource.Add(ChartTypeModelFactory.PotenciaStackedLine(Potencia, Fecha, 0xff7cb5ec));

                        //this line fixes swiping lag of the viewPager by caching the pages
                        ViewPager.OffscreenPageLimit = 2;
                        ViewPager.Adapter = new ViewPagerCons1Adapter(BaseContext, _chartTypesSource);
                        TabLayout.SetupWithViewPager(ViewPager);
                        progress.Dismiss();
                    }
                    catch (Exception ex)
                    {
                        progress.Dismiss();
                        Toast.MakeText(this, ex.Message, ToastLength.Short).Show();
                    }
                }
            }
        }
        //BackButton
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home)
            {
                Finish();
            }
            return base.OnOptionsItemSelected(item);
        }
    }
}