﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V4.View;
using SciChart.Charting.Visuals;
using SciChart.Charting.Visuals.Axes;
using SciChart.Charting.Modifiers;
using SciChart.Data.Model;
using SciChart.Drawing.Common;
using E7.Droid.Utiles;
using ML;

namespace E7.Droid.Consumo
{
    class ViewPagerCons2Adapter : PagerAdapter
    {
        private readonly Context _context;
        private readonly IList<ChartTypeModel> _chartTypesSource;

        public ViewPagerCons2Adapter(Context context, IList<ChartTypeModel> chartTypesSource)
        {
            _context = context;
            _chartTypesSource = chartTypesSource;
        }

        public override Java.Lang.Object InstantiateItem(ViewGroup container, int position)
        {
            var inflater = LayoutInflater.From(_context);
            var chartView = inflater.Inflate(Resource.Layout.ResourceCarrusel, container, false);

            var chartTypeModel = _chartTypesSource[position];

            UpdateSurface(chartTypeModel, chartView);

            container.AddView(chartView);

            return chartView;
        }

        private void UpdateSurface(ChartTypeModel chartTypeModel, View chartView)
        {
            var surface = (SciChartSurface)chartView.FindViewById(Resource.Id.chart);
            int themeid = Resource.Style.SciChart_Bright_Spark;
            surface.Theme = themeid;
            string title = chartTypeModel.Title.ToString();
            string m = chartTypeModel.Medida.ToString();

            var xAxis = new DateAxis(_context)
            {
                AxisTitle = "",
                CursorTextFormatting = "dd/MM/yy H:mm",
                TextFormatting = "dd. MMM",
                GrowBy = new DoubleRange(0, 0),
                DrawMajorGridLines = false,
                DrawMinorGridLines = false,
                AxisBandsStyle = new SolidBrushStyle(0xfff9f9f9),
            };
            var yAxis = new NumericAxis(_context)
            {
                AxisTitle = title,
                LabelProvider = new ThousandsLabelProvider(),
                GrowBy = new DoubleRange(0, 0.1d),
                CursorTextFormatting = "##,##0.00 " + m,
                DrawMajorGridLines = false,
                DrawMinorGridLines = false,
                AxisBandsStyle = new SolidBrushStyle(0xfff9f9f9),
                AxisAlignment = AxisAlignment.Left,
            };

            var seriesCollection = chartTypeModel.SeriesCollection;

            using (surface.SuspendUpdates())
            {
                surface.XAxes.Add(xAxis);
                surface.YAxes.Add(yAxis);
                surface.RenderableSeries.Add(seriesCollection);
                surface.ChartModifiers.Add(new CursorModifier
                {
                    ShowTooltip = true,
                });
                surface.ChartModifiers.Add(new PinchZoomModifier
                {
                    Direction = SciChart.Charting.Direction2D.XDirection,
                    IsUniformZoom = false,
                });
                surface.ChartModifiers.Add(new ZoomExtentsModifier());
                surface.ChartModifiers.Add(new ZoomPanModifier
                {
                    Direction = SciChart.Charting.Direction2D.XDirection,
                    ZoomExtentsY = false,
                    ClipModeX = SciChart.Charting.ClipMode.ClipAtExtents
                });
            }
        }

        public override void DestroyItem(ViewGroup container, int position, Java.Lang.Object @object)
        {
            container.RemoveView(container);
        }

        public override Java.Lang.ICharSequence GetPageTitleFormatted(int position)
        {
            var chartTypeModel = _chartTypesSource[position];
            return chartTypeModel.TypeName;
        }

        public override bool IsViewFromObject(View view, Java.Lang.Object @object)
        {
            return view == @object;
        }

        public override int Count => _chartTypesSource.Count;
    }

}