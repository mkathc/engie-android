﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Support.V4.Widget;
using Android.Support.Design.Widget;
using ML;
using ML.Usuario;
using E7.Entidades;

namespace E7.Droid.Consumo
{
    [Activity(Label = "PreConsActivity", Theme = "@style/Theme.DesignDemo", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class PreConsumo : Fragment
    {
        Button btn1, btn2;
        string _nombreCompleto, _filtro;
        FrameLayout fm1;
        RelativeLayout rlPrincipal;

        public PreConsumo(string nombre, string filtro)
        {
            _nombreCompleto = nombre;
            _filtro = filtro;
        }

        public event EventHandler CallConsumoFacturado;

        public void OnCallConsumoFacturado()
        {
            EventHandler handler = CallConsumoFacturado;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        public event EventHandler CallMedicionYGraficas;

        public void OnCallMedicionYGraficas()
        {
            EventHandler handler = CallMedicionYGraficas;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        public override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            return inflater.Inflate(Resource.Layout.PreConsumo, container, false);
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);

            btn1 = View.FindViewById<Button>(Resource.Id.button1);
            btn2 = View.FindViewById<Button>(Resource.Id.button2);
            fm1 = View.FindViewById<FrameLayout>(Resource.Id.fm1);
            rlPrincipal = View.FindViewById<RelativeLayout>(Resource.Id.rlPrincipal);
            btn1.Click += (sender, e) => Cons2();
            btn2.Click += (sender, e) => Cons1();

            var main = MainViewModel.GetInstance();
            int med = 0;

            ISharedPreferences pref = Application.Context.GetSharedPreferences("UserInfo", FileCreationMode.Private);
            var filtro = pref.GetString("filtro", "0");

            if (filtro != null && filtro != "0")
                main.Clientes = (List<BEClientes>)Newtonsoft.Json.JsonConvert.DeserializeObject<List<BEClientes>>(filtro);

            if (main.Clientes != null)
                foreach (var x in main.Clientes)
                    if (x.MedCliente != null)
                        foreach (var y in x.MedCliente)
                            med += 1;

            if (med == 0)
            {
                btn2.Visibility = ViewStates.Gone;
            }

            void Cons1()
            {
                OnCallConsumoFacturado();
            }
            void Cons2()
            {
                OnCallMedicionYGraficas();
            }
        }

        private int ConvertPixelsToDp(float pixelValue)
        {
            var dp = (int)((pixelValue) / Resources.DisplayMetrics.Density);
            return dp;
        }

    }
}